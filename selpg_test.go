package main

import "testing"

func TestInitflag(t *testing.T) {
	want := selpgArgs{0, 0, "", 72, false, ""}
	var got selpgArgs

	Initflag(&got)

	if got != want {
		t.Errorf("Initflag got %v, want %v\n", got, want)
	}
}

func TestProcessArgs(t *testing.T) {
	cases := []struct {
		insel selpgArgs
		inos  []string
		want  string
	}{
		{selpgArgs{1, 1, "test", 72, false, ""}, []string{"./selpg", "-s1", "-e1", "test"}, ""},
		{selpgArgs{0, 1, "test", 72, false, ""}, []string{"./selpg", "-s0", "-e1", "test"}, "./selpg: invalid start page 0\n"},
		{selpgArgs{2, 1, "test", 72, false, ""}, []string{"./selpg", "-s2", "-e1", "test"}, "./selpg: invalid end page 1\n"},
		{selpgArgs{1, 0, "", 72, false, ""}, []string{"./selpg", "-s1"}, "./selpg: not enough arguments\n"},
		{selpgArgs{0, 1, "test", 72, false, ""}, []string{"./selpg", "-e1", "-e1", "test"}, "./selpg: 1st arg should be -sstartPage\n"},
		{selpgArgs{1, 0, "test", 72, false, ""}, []string{"./selpg", "-s1", "-s1", "test"}, "./selpg: 2nd arg should be -eendPage\n"},
	}

	for _, c := range cases {
		got := ProcessArgs(&c.insel, c.inos)
		if got != c.want {
			t.Errorf("ProcessArgs(%v, %v) == %s, want %s\n", c.insel, c.inos, got, c.want)
		}
	}
}
