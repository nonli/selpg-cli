package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"os/exec"

	flag "github.com/spf13/pflag"
)

type selpgArgs struct {
	startPage  int
	endPage    int
	inFilename string
	pageLen    int
	pageType   bool
	printDest  string
}

var progname string
var stdin io.WriteCloser
var cmd *exec.Cmd

// Initflag initial flag and parse
func Initflag(sa *selpgArgs) {
	flag.IntVarP(&sa.startPage, "startPage", "s", 0, "Start page number")
	flag.IntVarP(&sa.endPage, "endPage", "e", 0, "End page number")
	flag.IntVarP(&sa.pageLen, "pageLen", "l", 72, "Line number for a page")
	flag.BoolVarP(&sa.pageType, "pageType", "f", false, "Determine form-feed-delimited")
	flag.StringVarP(&sa.printDest, "dest", "d", "", "Set printer")
	flag.Usage = usage

	flag.Parse()
}

func usage() {
	fmt.Printf("\nUSAGE: %s -sstartPage -eendPage [ -f | -llines_per_page ] [ -ddest ] [ inFilename ]\n", progname)
}

func main() {
	var sa selpgArgs

	Initflag(&sa)
	err := ProcessArgs(&sa, os.Args)
	if err != "" {
		fmt.Fprintf(os.Stderr, err)
		flag.Usage()
		os.Exit(1)
	}

	ans, err := ProcessInput(&sa)
	if err != "" {
		fmt.Fprintf(os.Stderr, err)
		os.Exit(1)
	} else {
		printAns(&sa, ans, stdin)
	}

	if sa.printDest != "" {
		stdin.Close()
		err := cmd.Run()
		if err != nil {
			fmt.Fprintf(os.Stderr, "\n%s: fail to connect to device\n", progname)
		}
	}
}

// ProcessArgs check args format
func ProcessArgs(sa *selpgArgs, args []string) string {
	progname = args[0]

	/* check the command-line arguments for validity */
	if len(args) < 3 {
		return fmt.Sprintf("%s: not enough arguments\n", progname)
	}

	/* handle 1st arg - start page */
	if args[1][1] != 's' {
		return fmt.Sprintf("%s: 1st arg should be -sstartPage\n", progname)
	}

	if sa.startPage < 1 || sa.startPage > (math.MaxInt32-1) {
		return fmt.Sprintf("%s: invalid start page %d\n", progname, sa.startPage)
	}

	/* handle 2nd arg - end page */
	num := 2
	if len(args[1]) == 2 {
		num = 3
	}
	if args[num][1] != 'e' {
		return fmt.Sprintf("%s: 2nd arg should be -eendPage\n", progname)
	}

	if sa.endPage < 1 || sa.endPage > (math.MaxInt32-1) || sa.startPage > sa.endPage {
		return fmt.Sprintf("%s: invalid end page %d\n", progname, sa.endPage)
	}

	var noerr string
	return noerr
}

// ProcessInput process input & output
func ProcessInput(sa *selpgArgs) (ans string, err string) {
	var ref string

	if sa.printDest != "" {
		var err1 error
		var err2 error
		cmd = exec.Command("cat")
		cmd.Stdout, err1 = os.OpenFile(sa.printDest, os.O_APPEND|os.O_WRONLY, os.ModeAppend)

		if err1 != nil {
			return "", fmt.Sprintf("\n%s: fail to open file %s\n", progname, sa.printDest)
		}

		stdin, err2 = cmd.StdinPipe()
		if err2 != nil {
			return "", fmt.Sprintf("\n%s: fail to open pipe to %s\n", progname, sa.printDest)
		}
	} else {
		stdin = nil
	}

	if flag.NArg() > 0 {
		sa.inFilename = flag.Arg(0)
		out, err := os.Open(sa.inFilename)
		if err != nil {
			return "", fmt.Sprintf("\n%s: fail to open file %s\n", progname, sa.inFilename)
		}

		reader := bufio.NewReader(out)
		if sa.pageType != false {
			for i := 1; i <= sa.endPage; i++ {
				line, err := reader.ReadString('\f')
				if err != io.EOF && err != nil {
					return "", fmt.Sprintf("\n%s: fail to read line\n", progname)
				}
				if err == io.EOF {
					break
				}
				if i >= sa.startPage {
					// printAns(sa, string(line), stdin)
					ref += string(line)
					ref += "\n"
				}
			}
		} else {
			count := 0
			for {
				line, _, err := reader.ReadLine()
				if err != io.EOF && err != nil {
					return "", fmt.Sprintf("\n%s: fail to read line\n", progname)
				}
				if err == io.EOF {
					break
				}
				if count/sa.pageLen+1 >= sa.startPage {
					if count/sa.pageLen+1 <= sa.endPage {
						// printAns(sa, string(line), stdin)
						ref += string(line)
						ref += "\n"
					} else {
						break
					}
				}

				count++
			}
		}
	} else {
		scanner := bufio.NewScanner(os.Stdin)
		count := 0
		str := ""
		for scanner.Scan() {
			line := scanner.Text()
			line += "\n"
			if count/sa.pageLen+1 >= sa.startPage {
				if count/sa.pageLen+1 <= sa.endPage {
					str += line
				}
			}

			count++
		}
		// printAns(sa, string(str), stdin)
		ref += string(str)
	}

	return ref, ""
}

func printAns(sa *selpgArgs, line string, stdin io.WriteCloser) {
	if sa.printDest != "" {
		stdin.Write([]byte(line + "\n"))
	} else {
		fmt.Println(line)
	}
}
